import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BookService} from './services/book/book.service';
import { BooksViewComponent } from './views/books-view/books-view.component';
import { BooksTableComponent } from './components/books-table/books-table.component';
import { BooksTableRowComponent } from './components/books-table-row/books-table-row.component';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { DetailBookViewComponent } from './views/detail-book-view/detail-book-view.component';
import { NotFoundViewComponent } from './views/not-found-view/not-found-view.component';
import {AuthService} from './services/auth/auth.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NewBookViewComponent } from './views/new-book-view/new-book-view.component';
import { EditBookViewComponent } from './views/edit-book-view/edit-book-view.component';
import { BookFormComponent } from './components/book-form/book-form.component';

@NgModule({
  declarations: [
    AppComponent,
    BooksViewComponent,
    BooksTableComponent,
    BooksTableRowComponent,
    AuthViewComponent,
    DetailBookViewComponent,
    NotFoundViewComponent,
    NewBookViewComponent,
    EditBookViewComponent,
    BookFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [BookService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
