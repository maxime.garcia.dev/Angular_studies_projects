import { Component, OnInit } from '@angular/core';
import {BookService} from '../../services/book/book.service';
import {ActivatedRoute} from '@angular/router';
import {Book} from '../../models/book.model';

@Component({
  selector: 'app-edit-book-view',
  templateUrl: './edit-book-view.component.html',
  styleUrls: ['./edit-book-view.component.css']
})
export class EditBookViewComponent implements OnInit {

  book: Book;
  msg: string;

  constructor(private bookService: BookService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;

    this.bookService.getBookById(+id)
      .then(book => this.book = book);
  }

  onSubmitEditBookForm(bookEdited: Book): void {
    this.bookService.edit(bookEdited)
      .then(msg => this.msg = msg);
  }

}
