import { Component, OnInit } from '@angular/core';
import {BookService} from '../../services/book/book.service';
import {Book} from '../../models/book.model';

@Component({
  selector: 'app-books-view',
  templateUrl: './books-view.component.html',
  styleUrls: ['./books-view.component.css']
})
export class BooksViewComponent implements OnInit {

  books: Array<Book>;

  constructor(private bookService: BookService) { }

  ngOnInit(): void {
    this.books = this.bookService.books;
  }

  onClickChangeAllStatus(newStatus: string): void {
    this.bookService.changeAllStatus(newStatus)
      .then(books => {
        this.books = books;
      });
  }
}
