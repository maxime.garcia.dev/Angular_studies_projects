import { Component, OnInit } from '@angular/core';
import {Book} from '../../models/book.model';
import {BookService} from '../../services/book/book.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-book-view',
  templateUrl: './new-book-view.component.html',
  styleUrls: ['./new-book-view.component.css']
})
export class NewBookViewComponent implements OnInit {

  book: Book;

  constructor(private bookService: BookService, private router: Router) {
    this.book = new Book(null, null, null, null, 'libre');
  }

  ngOnInit(): void {
  }

  onSubmitNewBookForm(bookToAdd: Book): void {
    this.bookService.add(bookToAdd)
      .then(() => this.router.navigateByUrl('/books'));
  }
}
