import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BookService} from '../../services/book/book.service';
import {Book} from '../../models/book.model';

@Component({
  selector: 'app-detail-book-view',
  templateUrl: './detail-book-view.component.html',
  styleUrls: ['./detail-book-view.component.css']
})
export class DetailBookViewComponent implements OnInit {

  book: Book;

  constructor(private route: ActivatedRoute, private bookService: BookService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;

    this.bookService.getBookById(+id)
      .then(book => this.book = book);
  }
}
