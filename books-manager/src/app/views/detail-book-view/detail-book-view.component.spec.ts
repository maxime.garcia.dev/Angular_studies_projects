import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailBookViewComponent } from './detail-book-view.component';

describe('DetailBookViewComponent', () => {
  let component: DetailBookViewComponent;
  let fixture: ComponentFixture<DetailBookViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailBookViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailBookViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
