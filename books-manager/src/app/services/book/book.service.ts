import { Injectable } from '@angular/core';
import {Book} from '../../models/book.model';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  books: Array<Book>;

  constructor() {
    this.books = [];

    for (let i = 1; i <= 10; i++){
      this.books.push(new Book(i,
        'Livre' + i,
        'Auteur' + i,
        'Description' + i,
        i % 2 === 0 ? 'libre' : 'pris')
      );
    }
  }

  changeAllStatus(newStatus: string): Promise<Array<Book>> {

    return new Promise<any>((res, rej) => {
      this.books.forEach(book => book.status = newStatus);
      res(this.books);
    });
  }

  changeStatus(bookId: number, newStatus: string): Promise<Book> {

    return new Promise<any>((res, rej) => {
      for (const [index, book] of this.books.entries()){
        if (book.id === bookId){
          this.books[index].status = newStatus;
          res(this.books[index]);
        }
      }
    });
  }

  getBookById(bookId: number): Promise<Book> {
    const cb = (res, rej) => {
      for (const book of this.books){
        if (book.id === bookId) {
          res(book);
        }
      }
    };
    return new Promise<any>(cb);
  }

  add(newBook: Book): Promise<void> {
    const cb = (res, rej) => {
      newBook.id = this.books[this.books.length - 1].id + 1;

      if (this.books.length === 0) {
        newBook.id = 1;
      }

      this.books.push(newBook);

      res();
    };

    return new Promise<void>(cb);
  }

  edit(bookEdited: Book): Promise<string> {

    const cb = (res, rej) => {
      for (const [index, book] of this.books.entries()){
        if (book.id === bookEdited.id){
          this.books[index] = bookEdited;
          res('Livre modifié avec succès :)');
        }
      }
    };
    return new Promise<string>(cb);
  }

  delete(bookId: number): void {
    for (const [index, book] of this.books.entries()){
      if (book.id === bookId){
        this.books.splice(index, 1);
        break;
      }
    }
  }
}
