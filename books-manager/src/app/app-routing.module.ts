import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BooksViewComponent} from './views/books-view/books-view.component';
import {AuthViewComponent} from './views/auth-view/auth-view.component';
import {AuthGuard} from './guards/auth/auth.guard';
import {NotFoundViewComponent} from './views/not-found-view/not-found-view.component';
import {DetailBookViewComponent} from './views/detail-book-view/detail-book-view.component';
import {NewBookViewComponent} from './views/new-book-view/new-book-view.component';
import {EditBookViewComponent} from './views/edit-book-view/edit-book-view.component';

const routes: Routes = [
  { path: '', component: AuthViewComponent},
  { path: 'books', canActivate: [AuthGuard], component: BooksViewComponent},
  { path: 'book/new', canActivate: [AuthGuard], component: NewBookViewComponent},
  { path: 'book/edit/:id', canActivate: [AuthGuard], component: EditBookViewComponent},
  { path: 'book/:id', canActivate: [AuthGuard], component: DetailBookViewComponent },
  { path: 'not-found', component: NotFoundViewComponent},
  { path: '**', redirectTo: 'not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
