import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Book} from '../../models/book.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  @Input() book: Book;
  @Input() submitLabel: string;

  @Output() submitForm: EventEmitter<Book>;

  bookForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.submitForm = new EventEmitter<Book>();
  }

  ngOnInit(): void {
    this._initForm();
  }

  onSubmitForm():
void {
    this.submitForm.emit(this.book);
  }

  private _initForm(): void {
    this.bookForm = this.fb.group({
      title: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
      author: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
      description: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(250)]],
      status: [null, [Validators.required]],
    });
  }
}
