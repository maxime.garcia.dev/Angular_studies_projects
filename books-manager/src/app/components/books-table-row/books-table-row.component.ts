import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {BookService} from '../../services/book/book.service';

@Component({
  selector: '[app-books-table-row]',
  templateUrl: './books-table-row.component.html',
  styleUrls: ['./books-table-row.component.css']
})
export class BooksTableRowComponent implements OnInit, OnChanges {

  @Input() id: number;
  @Input() title: string;
  @Input() author: string;
  @Input() status: string;

  nextStatus: string;

  constructor(private bookService: BookService) { }

  ngOnInit(): void {
  }

  ngOnChanges(): void{
    this.nextStatus = this.status === 'pris' ? 'libre' : 'pris';
  }

  onClickChangeBookStatus(): void {
   this.bookService.changeStatus(this.id, this.nextStatus)
     .then(book => {
       this.status = book.status;
       this.nextStatus = this.status === 'pris' ? 'libre' : 'pris';
     });
  }

  onClickDeleteBtn(): void {
    this.bookService.delete(this.id);
  }
}
